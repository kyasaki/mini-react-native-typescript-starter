# Mini ReactNative TypeScript Starter

## Installation

### 1. Android SDK
Plusieurs possiblités s'offrent à vous afin d'installer les outils nécessaire à l'execution de l'application:
 * **Expo** permet de partager l'application sur le réseau afin qu'elle soit accessible via l'application Expo, en revanche il devient impossible d'utiliser du code natif;
 * **Android Studio** est une usine à gaz qui pèse plusieurs gigaoctets, il contient une interface graphique permettant de manipuler le SDK, de créer des émulateurs, et d'assurer le transfert des données de debug via USB vers un terminal android externe.
 * **Android SDK** est la partie ligne de commande qui permet de faire excatement ce que fait Android Studio, mais avec uniquement le nécessaire pour que ça tourne en ligne de commande (donc c'est plus léger). Télécharger les SDK tools [ici](https://developer.android.com/studio/releases/platform-tools)

### 2. Paquet NPM
Pour installer le packet NPM, rien de plus facile:
```bash
npm i #install package
```

### 3. Choisir le nom de l'application
Modifier le nom de l'application dans le fichier [app.json](app.json), puis executer la commande suivante:
```bash
react-native eject #create native project
```

Le projet est prêt à être utilisé!

## Lancer l'application
Lancer le serveur de développement dans un terminal. Il faut le maintenir ouvert pendant le développement:
```bash
npm start
```

Puis installer l'application sur un périphérique (à faire une seule fois, ou à chaque fois qu'une dépendance native a été mise à jour; e.g. react-native-* dependencies):
```bash
npm run android #ou npm run ios
```

## ENJOY!
Les fichiers source se trouvent dans le dossier */src/*
